/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cardgame;

import java.util.ArrayList;

/**
 * represents the board on which is played on
 *
 * @author alexander.lohmann
 */
public class Gamefield {

    /**
     * represents the maximum Round count for this gamefield
     */
    private int maxRound;
    /**
     * represents the played playercars on the gamefield
     */
    private ArrayList<PlayerCard> playerCards;
    /**
     * represents the trump card on the gamefield
     */
    private Card trump;
    /**
     * represents the stack on the gamefield
     */
    private Cardstack defaultStack;

    /**
     * get the max round count
     *
     * @return max round count
     */
    public int getMaxRound() {
        return maxRound;
    }

    /**
     * get the played cards on the gamefield
     *
     * @return all played cards
     */
    public ArrayList<PlayerCard> getPlayerCards() {
        return playerCards;
    }

    /**
     * get the trump of the gamefield
     *
     * @return trump
     */
    public Card getTrump() {
        return trump;
    }

    /**
     * get the defaultstack of the gamefield
     *
     * @return cardstack
     */
    public Cardstack getDefaultStack() {
        return defaultStack;
    }

    /**
     * Sets the given defaultstack as the defaultstack of this gamefield
     *
     * @param defaultStack
     */
    public void setDefaultStack(Cardstack defaultStack) {
        this.defaultStack = defaultStack;
    }

    /**
     * Checking amount of Points for each Player and consoleoutputs the winner.
     *
     * @param player1Points
     * @param player2Points
     * @return String of the Player who has won
     */
    public String checkWhoWon(int player1Points, int player2Points) {
        if (player1Points > player2Points) {
            return("Spieler 1 hat gewonnen!");
        } else {
            if (player1Points == player2Points) {
                return("Unentschieden!");
            } else {
                return("Spieler 2 hat gewonnen!");
            }
        }
    }

    /**
     * Asking user for Max round number and setting it .
     */
    public void askAndSetMaxRounds() {
        
    }

    /**
     * Give out the amount of Cards corresponding with Number of Currentround to
     * all Players.
     *
     * @param currentRound
     * @param player
     */
    public void giveCardsForCurrentRound(int currentRound, Player player) {
        for (int i = 0; i  < currentRound; i++) {
            player.addToPlayerHand(defaultStack.takeAndRemoveCard());
        }
    }

    /**
     * Consoleoutput of all Cards on the Board.
     *
     * @param playerlist
     * @return 
     */
    public String showPlayedCardsAndTrump(ArrayList<Player> playerlist) {
        for (Player player : playerlist) {
            if (!playerlist.isEmpty() || playerlist.size() < 2) {
                return("\n\n" + player + "Card= " + playerCards.get(0).getCard().toColoredString());
            }
            return("\nGezogener Trumpf: " + trump.toColoredString());
        }
        return null;
    }

    /**
     * Put stack on field.
     */
    public void cardStackToField() {
        defaultStack = Cardstack.buildAndShuffleDefaultStack();
    }

    /**
     * Get Card from stack and add it to trump.
     */
    public void settingTrump() {
        this.trump = defaultStack.takeAndRemoveCard();
    }

    /**
     * Consoleoutput of start of the round.
     */
    public String showRoundStart() {
        return("######################[Start : Round \n");
    }

    /**
     * Consoleoutput of end of the round.
     * @return 
     */
    public String showRoundEnd() {
        return("Round End ]######################\n\n");
    }

    /**
     * Checking played Cards and deciding who has won.
     *
     * @param playercard1
     * @param playercard2
     * @return the Playercard of who has won for next subSubround
     */
    public PlayerCard subSubRoundWinner(PlayerCard playercard1, PlayerCard playercard2) {
        if (playercard1.getCard().getColor() == playercard2.getCard().getColor()) {
            if (playercard1.getCard().getValue() > playercard2.getCard().getValue()) {
                return playercard1;
            } else {
                return playercard2;
            }
        } else {
            if (playercard2.getCard().getColor() == trump.getColor()) {
                return playercard2;
            } else {
                return playercard1;
            }
        }
    }

}
