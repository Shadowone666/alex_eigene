/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cardgame;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 *
 * @author alexander.lohmann
 */
// TODO: Demo von Olli
public class Game {

    private int currentRound;
    private int playerCount;
    private final ArrayList<Player> orderedPlayers = new ArrayList<>();
    private final StringBuilder gameOutput = new StringBuilder();
    private int maxRounds;

    public Game(int playerCount, int maxRounds) {
        this.playerCount = playerCount;
        this.maxRounds = maxRounds;
    }

    /**
     * Plays the game, must only be called once.
     */
    public void play() {

        for (int i = 1; i <= playerCount; i++) {
            Player player = new Player("Bot " + i);
            orderedPlayers.add(player);
            gameOutput.append("Added player: ").append(player.getName()).append("\n");
        }

        //erstellung neues Spielfeld
        Gamefield game1 = new Gamefield();
        //Stapel wird aufs Spielfeld gelegt
        game1.cardStackToField();
        //Start der Runde
        for (currentRound = 1; currentRound <= maxRounds; currentRound++) {
            //Konsolenausgabe von Rundenanfang
            gameOutput.append(game1.showRoundStart());

            //Überprüfung Rundenzahl und Verteilung Karten nach Rundenzahl
            for (Player player : orderedPlayers) {
                game1.giveCardsForCurrentRound(currentRound, player);
            }
            //festlegung Trumpf
            game1.settingTrump();

            //Spieler anordnen für die 1. Runde
            //1. Karte jedes Spielers anzeigen
            ArrayList<Player> contestants = new ArrayList<>();
            for (Player p : orderedPlayers) {
                contestants.add(p);
            }

            //Anfang Subrunde
            for (int i = 1; i <= currentRound; i++) {
                gameOutput.append("Trump= ").append(game1.getTrump().toColoredString()).append("\n");
                /*
                contestant one chooses card. this instance is overridden by the winner every match.
                 */
                Player contestantOne = contestants.get(0);
                contestants.remove(0);
                PlayerCard contestantOneCard = new PlayerCard(contestantOne, contestantOne.playCard(0));

                //Anfang Hahnenkämpfe
                for (int j = 1; j < playerCount; j++) {
                    gameOutput.append(contestantOne.getName()).append(" played ").append(contestantOneCard.getCard().toColoredString()).append("\n");
                    //next player is choosen and removed from contestants
                    Player contestantTwo = contestants.get(0);
                    contestants.remove(0);

                    //next player choose card
                    PlayerCard contestantTwoCard = new PlayerCard(contestantTwo, contestantTwo.playCard(0));
                    gameOutput.append(contestantTwo.getName()).append(" played ").append(contestantTwoCard.getCard().toColoredString()).append("\n\n");
                    //evaluate and save winner
                    contestantOneCard = game1.subSubRoundWinner(contestantOneCard, contestantTwoCard);

                    //Siegerkarte anzeigen
                    //System.out.println(contestantOneCard.getCard().toColoredString());
                }

                //add tuck to winner
                contestantOneCard.getPlayer().addPlayerTuck();

                //Neubefüllung der geordneten Spielerliste nach Gewinner
                int ordinalnumber = orderedPlayers.indexOf(contestantOneCard.getPlayer());
                contestants = new ArrayList<>(orderedPlayers.subList(ordinalnumber, orderedPlayers.size()));
                contestants.addAll(orderedPlayers.subList(0, ordinalnumber));
                //String contestantString = contestants.stream().map(s -> s.getName()).collect(Collectors.joining(","));
                //gameOutput.append("Teilnehmer= ").append(contestantString).append("\n");
            }

            //Konsolenausgabe von Rundenende
            gameOutput.append(game1.showRoundEnd());
            //Erneutes füllen des Kartenstapels und mischen für neue Runde
            game1.setDefaultStack(Cardstack.buildAndShuffleDefaultStack());
            //neue Reihenfolge der Spieler für nächste Runde
            orderedPlayers.add(orderedPlayers.remove(0));
        }
        //überprüfung wer mehr Punkte hat
        //gameOutput.append(game1.checkWhoWon(game1.getPlayer1().getPoints(), game1.getPlayer2().getPoints()));

    }

    /**
     * Returns a multiline String representing the result, can only be called
     * after play().
     *
     * @return
     */
    public String getResult() {
        return gameOutput.toString();
    }

}
