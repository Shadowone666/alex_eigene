/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cardgame;

import java.awt.Color;

/**
 * represents the colors that are allowed for creating cards
 *
 * @author alexander.lohmann
 */
public enum AllowedColor {

    RED(Color.RED, "[31m"),
    BLUE(Color.BLUE, "[34m"),
    GREEN(Color.GREEN, "[32m"),
    YELLOW(Color.YELLOW, "[33m"),
    BLACK(Color.BLACK, "[30m"),
    RESET(null, "[0m");
    /**
     * represents the color of allowedcolors
     */
    private final Color color;
    /**
     * represents the ansicode of allowedcolors
     */
    private final String ansiCode;

    /**
     * creates a new instance of allowedcolor
     *
     * @param color
     * @param ansiCode
     */
    private AllowedColor(Color color, String ansiCode) {
        this.color = color;
        this.ansiCode = ansiCode;
    }

    /**
     * get the color of allowedcolors
     *
     * @return the color
     */
    public Color getColor() {
        return color;
    }

    /**
     * get the ansicode of allowedcolors
     *
     * @return the ansicode
     */
    public String getAnsiCode() {
        return ansiCode;
    }

    /**
     * Returns a color code mostly used to colorize the console output.
     *
     * @return a \033 escaped ansi color code.
     */
    public String toAnsiColor() {
        return "\033" + ansiCode;
    }

    /**
     * String that contains the color and the ansicode
     *
     * @return a String with the color and ansicode
     */
    @Override
    public String toString() {
        return "AllowedColor{" + "color=" + color + ", ansiCode=" + ansiCode + '}';
    }
}
