/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cardgame;

/**
 * Represents a Card that has a color and a value
 * @author alexander.lohmann
 */
public class Card {
    /**
    * Represents the value of the Card
    */
    private final int value;
    /**
     * represents the Color of the Card
     */
    private final AllowedColor color;
    /**
    * creates a new instance of Card
    * @param value
    * @param color 
    */
    public Card(int value, AllowedColor color) {
        this.value = value;
        this.color = color;
    }
    /**
    * Get the value of the Card
    * @return the value of the Card 
     */
    public int getValue() {
        return value;
    }
    /**
    * Get the color of the Card
    * @return color of the Card 
    */
    public AllowedColor getColor() {
        return color;
    }
    /**
    * String colors in Card color with Card Attributes.
    * @return   String that is Colored in Console.
    */
    public String toColoredString() {
        return color.toAnsiColor() + "[" + color.name() + "] [" + value + "]" + AllowedColor.RESET.toAnsiColor();
    }
    /**
    * String containing the value and Color of Card
    * @return a String containing the value and color of Card 
    */
    @Override
    public String toString() {
        return "Card{" + "value=" + value + ", color=" + color + '}';
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
//    public String rgbToString() {
//        String returnstring = " ";
//        if (color.getAnsiCode() == "[31m") {
//            returnstring = color.toAnsiColor() + "[Red]" + "[" + value + "]";
//        } else if (color.getAnsiCode() == "[34m") {
//            returnstring = color.toAnsiColor() + "[Blue]" + "[" + value + "]";
//        } else if (color.getAnsiCode() == "[32m") {
//            returnstring = color.toAnsiColor() + "[Green]" + "[" + value + "]";
//        } else if (color.getAnsiCode() == "[33m") {
//            returnstring = color.toAnsiColor() + "[Yellow]" + "[" + value + "]";
//        } else {
//            System.out.println(color.toAnsiColor() + "error");
//        }
//        return returnstring;
//    }
}
