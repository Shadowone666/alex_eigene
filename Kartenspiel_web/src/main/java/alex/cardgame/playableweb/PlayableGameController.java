/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alex.cardgame.playableweb;

import alex.cardgame.game.Card;
import alex.cardgame.game.Game;
import alex.cardgame.game.GameResult;
import alex.cardgame.game.Gamefield;
import alex.cardgame.game.Player;
import alex.cardgame.game.PlayerCard;
import alex.cardgame.game.RoundResult;
import alex.cardgame.game.SubRoundResult;
import java.io.Serializable;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.ToggleEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author alexander.lohmann
 */
@Named
@ViewScoped
public class PlayableGameController implements Serializable {

    /**
     * Information Logger
     */
    private final static Logger L = LoggerFactory.getLogger(PlayableGameController.class);

    /**
     * Boolean for check if the setup is complete to not render setup elements
     * when complete.
     */
    private boolean rendered = true;
    /**
     * Boolean to set to true to render the Cards of the Player to choose from.
     */
    private boolean cardRendered = false;
    /**
     * Boolean to set to true to render the Card on the Field to choose your
     * card to play after.
     */
    private boolean playedCardRendered = false;
    /**
     * Boolean to set to true after an interaction of a Player.
     */
    private boolean nextbuttonRendered = false;
    /**
     * Saves Results of this Game.
     */
    private GameResult gameResult;
    /**
     * Saves the Results of this SubRound
     */
    private SubRoundResult subroundresult;
    /**
     * Saves the Result of this Round.
     */
    private RoundResult roundresult;
    /**
     * Gamefield on which current Game is played on.
     */
    private Gamefield gamefield;
    /**
     * Errormessage for not enough Bots.
     */
    private String errormessage = "";
    /**
     * Name of the Player.
     */
    private String name;
    /**
     * Number of user defined maxRounds.
     */
    private int maxRounds;
    /**
     * Integer that holds the currentRound Count
     */
    private int currentRound;
    /**
     * Integer that holds the currentSubRound Count
     */
    private int currentSubRound;
    /**
     * Integer that holds the currentFight Count
     */
    private int currentFight;
    /**
     * Hold the Player whose turn it is.
     */
    private Player currentPlayer;
    /**
     * First Contestant of this Fight.
     */
    private PlayerCard contestantOneCard;
    /**
     * Second Contestant of this Fight.
     */
    private PlayerCard contestantTwoCard;
    /**
     * Contestant One of the current Fight.
     */
    private Player contestantOne;
    /**
     * Contestant Two of the current Fight.
     */
    private Player contestantTwo;
    /**
     * Game and gamelogic.
     */
    private Game game;

    /**
     * For Testing Purposes only
     */
    private String testOutput;
    /**
     * Holds all the Players of the current Subround.
     */
    private ArrayList<Player> contestants;

    /**
     * Boolean to set to true after the Gamesetup is finalized.
     *
     * @return true if gamesetup is not yet finalized and false for finalized.
     */
    public boolean isRendered() {
        return rendered;
    }

    /**
     * Getter for Boolean to set to true to render the Cards of the Player to
     * choose from.
     *
     * @return Boolean of the current status of the Cards true = rendered false
     * = not rendered
     */
    public boolean isCardRendered() {
        return cardRendered;
    }

    /**
     * Getter for Boolean to set to true to render the Card on the Field to
     * choose your card to play after.
     *
     * @return Boolean of the Card on the Field status of the Cards true =
     * rendered false = not rendered
     */
    public boolean isPlayedCardRendered() {
        return playedCardRendered;
    }

    /**
     * Returns True after an interaction of a Player. And False when there was
     * no interaction with a Player yet.
     *
     * @return true after an interaction of a Player. And False when there was
     * no interaction with a Player yet.
     */
    public boolean isNextbuttonRendered() {
        return nextbuttonRendered;
    }

    /**
     * Saves Results of this Game.
     *
     * @return result of this Game.
     */
    public GameResult getGameResult() {
        return gameResult;
    }

    /**
     * Gamefield on which current Game is played on.
     *
     * @return Gamefield on which current Game is played on.
     */
    public Gamefield getGamefield() {
        return gamefield;
    }

    /**
     * Returns string for not enough Bots.
     *
     * @return String representing errormessage.
     */
    public String geterrormessage() {
        return errormessage;
    }

    /**
     * User defined Name of the Player.
     *
     * @return Name of the Player.
     */
    public String getName() {
        return name;
    }

    /**
     * User defined amount of Maxrounds.
     *
     * @return number of user defined maxrounds.
     */
    public int getMaxRounds() {
        return maxRounds;
    }

    /**
     * Getter for the Player who has to choose a Card.
     *
     * @return Player who has to choose a Card.
     */
    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    /**
     * Returns First Contestant of this Fight.
     *
     * @return First Contestant of this Fight.
     */
    public PlayerCard getContestantOneCard() {
        return contestantOneCard;
    }

    /**
     * Returns Second Contestant of this Fight.
     *
     * @return Second Contestant of this Fight.
     */
    public PlayerCard getContestantTwoCard() {
        return contestantTwoCard;
    }

    /**
     * Returns current game instance.
     *
     * @return current game.
     */
    public Game getGame() {
        return game;
    }

    /**
     * Retursn String that contains Testing Information.
     *
     * @return String Containing Message for testing.
     */
    public String getTestOutput() {
        return testOutput;
    }

    /**
     * Sets the Name of the Player.
     *
     * @param name
     */
    public void setname(String name) {
        this.name = name;
    }

    /**
     * Sets the amount of MaxRounds.
     *
     * @param maxRounds
     */
    public void setMaxRounds(int maxRounds) {
        this.maxRounds = maxRounds;
    }

    /**
     * Info for game config is closed.
     *
     * @param event
     */
    public void onClose(CloseEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Panel Closed", "Closed panel id:'" + event.getComponent().getId() + "'");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Info for game config is opened.
     *
     * @param event
     */
    public void onToggle(ToggleEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, event.getComponent().getId() + " toggled", "Status:" + event.getVisibility().name());
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Setup for the current game with bot amount check.
     */
    public void setupMaxRounds() {
        game.setMaxRounds(maxRounds);
        testOutput = "<br/> Maximum Rounds set to " + maxRounds + ".";
    }

    /**
     * Method to initialize Game.
     */
    @PostConstruct
    public void createGame() {
        this.game = new Game();
        currentRound = 1;
        currentSubRound = 1;
        currentFight = 1;
        gameResult = new GameResult();
    }

    /**
     * Sets the Name to an Empty string when Null and creates a Player.
     */
    public void addPlayer() {
        if (name == null) {
            name = "";
        }
        game.createHumanPlayer(name);
        testOutput = "<br/>" + game.getOrderedPlayers().get(game.getOrderedPlayers().size() - 1).getName() + " hinzugefügt";
    }

    /**
     * Takes the Parameters of the Setup and Starts the first Round.
     */
    public void finalizeSetup() {
        if (game.getOrderedPlayers().size() < 2) {
            testOutput = "<br/> Please Setup  combined at least 2 Player or Bot<br/>";
        } else if (game.getMaxRounds() == 0) {
            testOutput = "<br/> Please Setup Round Count first <br/>";
        } else {
            //Generating new Gamefield.
            gamefield = new Gamefield();
            //Putting Cardstack on field
            gamefield.cardStackToField();
            testOutput = "";
            rendered = false;
            //saves information about this round in roundresults,sets Trump and hands out cards to each player.
            roundresult = game.prepareForRound(gamefield, currentRound);
            //New PlayerOrder for current Round.
            contestants = game.setsPlayerListForRound();
            //saves some information about this subround in subroundresults.
            subroundresult = game.prepareForSubRound(roundresult, currentSubRound);
            //first contestant
            contestantOne = game.setUpAnContestant(contestants);
            if (contestantOne.isBot()) {
                botIsContestantOne();
            } else {
                playerIsContestantOne();
            }
        }
    }

    /**
     * Checks if the currentPlayer is the first or second and plays the Card.
     * Sets the Played Card and corresponding Player as ContestantOne or Two
     * depending on first or second to play the Card.
     *
     * @param card Card that the Contestant wants to Play.
     */
    public void contestantPlaysCard(Card card) {
        if (contestantOne.equals(currentPlayer)) {
            contestantOneCard = game.setUpAnContestantPlayerCard(currentPlayer, card);
            System.out.println("ContestantOne= " + contestantOne.getName() + " Played his/her Card.");
            System.out.println("Card played =" + card.getColor().name() + " " + card.getValue());
            cardRendered = false;
            nextbuttonRendered = true;
            playedCardRendered = false;
            testOutput = "";
        } else {
            contestantTwoCard = game.setUpAnContestantPlayerCard(currentPlayer, card);
            System.out.println("ContestantTwo= " + contestantTwo.getName() + " Played his/her Card.");
            System.out.println("Card played =" + card.getColor().name() + " " + card.getValue());
            cardRendered = false;
            nextbuttonRendered = true;
            playedCardRendered = false;
            testOutput = "";
        }
    }

    public void nextToPlayCard() {
        if (contestantOne.equals(currentPlayer)) {
            if (!contestants.isEmpty()) {
                if (contestantTwo.isBot()) {
                    botIsContestantTwoAndWinnerDecided();
                } else {
                    playerIsContestantTwo();
                }
            } else {
                if (contestantOne.getPlayerHand().size() != contestantTwo.getPlayerHand().size()) {
                    if (contestantTwo.isBot()) {
                        botIsContestantTwoAndWinnerDecided();
                    } else {
                        playerIsContestantTwo();
                    }
                } else {
                    prepareAndCheckForNextSubround();
                }
            }
        } else if (contestantTwo.equals(currentPlayer)) {
            if ((contestants.size()) > 0) {
                currentFight = currentFight + 1;
                contestantTwo = game.setUpAnContestant(contestants);
                if (contestantTwo.isBot()) {
                    botIsContestantTwoAndWinnerDecided();
                } else {
                    playerIsContestantTwo();
                }
            } else if (contestantOne.getPlayerHand().size() == contestantTwo.getPlayerHand().size()) {
                winnerIsDetermined();
                if (contestants.isEmpty()) {
                    prepareAndCheckForNextSubround();
                }
            } else {
                if (contestantOne.isBot()) {
                    botIsContestantOne();
                } else {
                    playerIsContestantOne();
                }
            }
        } else {
            if ((contestants.size()) > 0) {
                contestantTwo = game.setUpAnContestant(contestants);
                currentFight = currentFight + 1;
                if (contestantTwo.isBot()) {
                    botIsContestantTwoAndWinnerDecided();
                } else {
                    playerIsContestantTwo();
                }
            } else {
                prepareAndCheckForNextSubround();
            }
        }

        //Ausgabe des Gewinners
    }

    private void botIsContestantTwoAndWinnerDecided() {
        contestantTwoCard = game.setUpAnContestantPlayerCard(contestantTwo);
        System.out.println("ContestantTwo= " + contestantTwoCard.getPlayer().getName() + " Played his/her Card.");
        System.out.println("Card played =" + contestantTwoCard.getCard().getColor().name() + " " + contestantTwoCard.getCard().getValue());
        nextbuttonRendered = true;
        playedCardRendered = false;
        currentPlayer = new Player("");
        System.out.println("The Winner with an Bot is determined.");
        contestantOneCard = game.runFight(contestantOneCard, contestantTwoCard, gamefield.getTrump(), subroundresult, currentFight);
    }

    private void botIsContestantOne() {
        contestantOneCard = game.setUpAnContestantPlayerCard(contestantOne);
        System.out.println("ContestantOne= " + contestantOneCard.getPlayer().getName() + " Played his/her Card.");
        System.out.println("Card played =" + contestantOneCard.getCard().getColor().name() + " " + contestantOneCard.getCard().getValue());
        nextbuttonRendered = true;
        contestantTwo = new Player("");
        currentPlayer = new Player("");
    }

    private void playerIsContestantTwo() {
        System.out.println("CurrentPlayer is set to contestantTwo");
        currentPlayer = contestantTwo;
        cardRendered = true;
        playedCardRendered = true;
        nextbuttonRendered = false;
    }

    private void playerIsContestantOne() {
        currentPlayer = contestantOne;
        cardRendered = true;
        nextbuttonRendered = false;
        System.out.println("CurrentPlayer is contestantOne and it is rendered.");
        if (!contestants.isEmpty()) {
            contestantTwo = game.setUpAnContestant(contestants);
        }

    }

    private void winnerIsDetermined() {
        cardRendered = false;
        playedCardRendered = false;
        nextbuttonRendered = false;
        System.out.println("The Winner is determined.");
        contestantOneCard = game.runFight(contestantOneCard, contestantTwoCard, gamefield.getTrump(), subroundresult, currentFight);

    }

    private void prepareAndCheckForNextSubround() {
        contestantOneCard.getPlayer().addPlayerPoint();
        testOutput = contestantOneCard.getPlayer().getName() + " got a Point";
        nextbuttonRendered = false;
        //Nächste Subrunde?
        game.finishSubRound(contestantOneCard, roundresult, subroundresult);
        currentSubRound = currentSubRound + 1;
        System.out.println("Current SubRound= " + currentSubRound);
        if (currentSubRound <= currentRound) {
            prepareForSubRound();
        } else {
            contestants = game.giveListForNextSubRound(contestantOneCard, contestants);
            currentRound = currentRound + 1;
            System.out.println("Current Round =" + currentRound);
            if (currentRound <= maxRounds) {
                prepareForRound();
            } else {
                cardRendered = false;
                playedCardRendered = false;
                nextbuttonRendered = false;
                testOutput = "Irgendwer hat Gewonnen mir grad noch egal wer";

            }
        }
    }

    private void prepareForSubRound() {
        System.out.println("Next SubRound is started ----------------------------");
        contestants = game.giveListForNextSubRound(contestantOneCard, contestants);
        if (contestantOne.isBot()) {
            botIsContestantOne();
        } else {
            playerIsContestantOne();
        }
        subroundresult = game.prepareForSubRound(roundresult, currentSubRound);
        contestantOne = game.setUpAnContestant(contestants);
    }

    private void prepareForRound() {
        currentSubRound = 1;
        System.out.println("Next Round is started ++++++++++++++++++++++++++++");
        System.out.println("CurrentSubRound is reseted = CurrentSubRound:" + currentSubRound);
        roundresult = game.prepareForRound(gamefield, currentRound);
        contestants = game.setsPlayerListForRound();
        subroundresult = game.prepareForSubRound(roundresult, currentSubRound);
        contestantOne = game.setUpAnContestant(contestants);
        if (contestantOne.isBot()) {
            botIsContestantOne();
        } else {
            playerIsContestantOne();
        }
    }
}
