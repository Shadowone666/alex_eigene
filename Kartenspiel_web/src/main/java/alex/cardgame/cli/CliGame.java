/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alex.cardgame.cli;

import alex.cardgame.game.Game;

/**
 *
 * @author alexander.lohmann
 */
public class CliGame {

    public static void main(String[] args) {
        int bots, maxRounds;
        /*
        
        Make sure at least two players/bots are playing
        
         */
        // ---- Phase Eingabe
        do {
            //Abfrage Anzahl Bots
            System.out.println("Bitte Bot Anzahl Eingeben.");
            bots = new java.util.Scanner(System.in).nextInt();
        } while (bots < 2);

        // 
        //Erstellung Anzahl Bots als Spielerobjekte und Speicherung in ArrayList
        System.out.println("Bitte Runden Anzahl Eingeben.");
        maxRounds = new java.util.Scanner(System.in).nextInt();
        // ----- Ende Phase Eingabe

        Game game = new Game(bots, maxRounds);
        game.createPlayers();
        game.play();
        String out = game.getResult();
        System.out.println(out);

    }

}
