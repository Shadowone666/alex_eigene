/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alex.cardgame.simpleweb;

import alex.cardgame.game.Game;
import alex.cardgame.game.GameResult;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.ToggleEvent;

/**
 * Class to be able to run Cardgame on Webserver.
 *
 * @author alexander.lohmann
 */
@Named
@ViewScoped
public class SimpleGameController implements Serializable {

    private GameResult gameresult = new GameResult();

    public GameResult getGameresult() {
        return gameresult;
    }

    public void setGameresult(GameResult gameresult) {
        this.gameresult = gameresult;
    }

    /**
     * Errormessage for not enough Bots.
     */
    private String errormessage = "";
    /**
     * Number of user defined Bots.
     */
    private int bots;
    /**
     * Number of user defined maxRounds.
     */
    private int maxRounds;
    /**
     * Botnumber to create game.
     */
    private int bt;
    /**
     * Maxroundsnumber to create game.
     */
    private int mr;
    /**
     * Output of the whole game procedure as a string.
     */
    private String gameoutput = "";
    /**
     * Game and gamelogic.
     */
    private Game game;

    /**
     *
     * @return String representing whole Cardgame procedure.
     */
    public String getgameoutput() {
        String newgameoutput = gameoutput.replaceAll("\\n", "<br />");
        newgameoutput = newgameoutput.replaceAll("\\033", "");
        newgameoutput = newgameoutput.replaceAll("\\[0m", " ");
        newgameoutput = newgameoutput.replaceAll("\\[33m", " ");
        newgameoutput = newgameoutput.replaceAll("\\[34m", " ");
        newgameoutput = newgameoutput.replaceAll("\\[31m", " ");
        newgameoutput = newgameoutput.replaceAll("\\[32m", " ");
        gameoutput = "";
        return newgameoutput;
    }

    /**
     * Returns current game instance.
     *
     * @return current game.
     */
    public Game getGame() {
        return game;
    }

    /**
     * Returns string for not enough Bots.
     *
     * @return String representing errormessage.
     */
    public String geterrormessage() {
        return errormessage;
    }

    /**
     * User defined amount of Bots.
     *
     * @return number of user defined Bots.
     */
    public int getBots() {
        return bots;
    }

    /**
     * User defined amount of Maxrounds.
     *
     * @return number of user defined maxrounds.
     */
    public int getMaxRounds() {
        return maxRounds;
    }

    /**
     * Sets the amount of Bots.
     *
     * @param bots
     */
    public void setBots(int bots) {
        this.bots = bots;
    }

    /**
     * Sets the amount of MaxRounds.
     *
     * @param maxRounds
     */
    public void setMaxRounds(int maxRounds) {
        this.maxRounds = maxRounds;
    }

    /**
     * Info for game config is closed.
     *
     * @param event
     */
    public void onClose(CloseEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Panel Closed", "Closed panel id:'" + event.getComponent().getId() + "'");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Info for game config is opened.
     *
     * @param event
     */
    public void onToggle(ToggleEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, event.getComponent().getId() + " toggled", "Status:" + event.getVisibility().name());
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Setup for the current game with bot amount check.
     */
    public void gameSetup() {
        if (bots < 2) {
            errormessage = "Please Input at least 2 Bots.";
        } else {
            errormessage = null;
            mr = maxRounds;
            bt = bots;
        }
    }

    /**
     * Runs the game and writes it in gameoutput.
     */
    public void mains() {
        game = new Game(bt, mr);
        game.createPlayers();
        game.play();
        gameoutput = game.getResult();
        game.resetStringBuilder();
        System.out.println(gameoutput);
        gameresult = game.getDetailResult();

    }
}
