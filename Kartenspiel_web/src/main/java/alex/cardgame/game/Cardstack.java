/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alex.cardgame.game;

import java.util.*;

/**
 * Represents a Stack that contains cards.
 *
 * @author alexander.lohmann
 */
public class Cardstack {

    /**
     * represents a list containing the allowed colors
     */
    private final List<AllowedColor> allowedColors;
    /**
     * represents the stack of the cardstack
     */
    private final List<Card> stack;

    /**
     * creates new instance of Cardstack
     */
    public Cardstack() {
        this.stack = new ArrayList<>();
        this.allowedColors = new ArrayList<>();
    }

    /**
     * get all allowed colors of the cardstack
     *
     * @return all allowed colors for the cardstack
     */
    public List<AllowedColor> getAllowedColors() {
        return allowedColors;
    }

    /**
     * TODO: make the stack unmodifiable Get the stack of the cardstack.
     *
     * @return the cards in this stack.
     */
    public List<Card> getStack() {
        return stack;
    }

    /**
     * generates a cardstack with Values of up to 13 in colors
     * red,blue,green,yellow
     *
     * @return an Cardstack
     */
    public static Cardstack buildDefaultCardstack() {
        return buildCardstack(13,
                AllowedColor.RED,
                AllowedColor.BLUE,
                AllowedColor.GREEN,
                AllowedColor.YELLOW
        );
    }

    /**
     * builds a defaultcardstack and shuffles it
     *
     * @return shuffled default Cardstack
     */
    public static Cardstack buildAndShuffleDefaultStack() {
        Cardstack stacks = buildDefaultCardstack();
        Collections.shuffle(stacks.getStack());
        return stacks;
    }

    /**
     * builds a Cardstack with user defined Parameters.
     *
     * @param maxCardValue
     * @param allowedStackColors
     * @return Cardstack with now defined Parameters
     */
    public static Cardstack buildCardstack(int maxCardValue, AllowedColor... allowedStackColors) {
        Cardstack cardstack = new Cardstack();
        cardstack.allowedColors.addAll(Arrays.asList(allowedStackColors));

        for (AllowedColor defaultColor : allowedStackColors) {
            for (int s = 1; s <= maxCardValue; s++) {
                cardstack.stack.add(new Card(s, defaultColor));
            }
        }
        return cardstack;
    }

    /**
     * String with the contents of the cardstack
     *
     * @return String
     */
    @Override
    public String toString() {
        return "Cardstack{colors=" + allowedColors + ", stack=" + stack + "}";
    }

    /**
     * Take Card that is on top of the stack from it and returns it
     *
     * @return the card that was on top of the stack
     */
    public Card takeAndRemoveCard() {
        Card blub = this.stack.get(0);
        this.stack.remove(0);
        return blub;
    }
}
