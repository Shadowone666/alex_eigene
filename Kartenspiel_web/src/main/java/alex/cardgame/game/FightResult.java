/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alex.cardgame.game;

import java.util.ArrayList;
import java.util.List;

/**
 * Representing the Results for a single Fight.
 * @author alexander.lohmann
 */
public class FightResult {

    /**
     * Name of the current Fight
     */
    private String currentFight;


    /**
     * Contains all PlayerCards for a single Fight.
     */
    private List<PlayerCard> playerCards = new ArrayList<>();
    /**
     * Contains the winner for a single Fight.
     */
    private PlayerCard winner;

    /**
     * return String with name of currentFight
     *
     * @return String with name of currentFight
     */
    public String getCurrentFight() {
        return currentFight;
    }

    /**
     * returns list of Playercards for current Fight.
     *
     * @return list of Playercards for current Fight.
     */
    public List<PlayerCard> getPlayerCards() {
        return playerCards;
    }

    /**
     * returns the winner Playercard for current Fight.
     *
     * @return the winner Playercard for current Fight.
     */
    public PlayerCard getWinner() {
        return winner;
    }
    /**
     * Sets the defined name of current Fight.
     * @param currentFight 
     */
    public void setCurrentFight(String currentFight) {
        this.currentFight = currentFight;
    }
    /**
     * Sets Fight with defined PlayerCards.
     *
     * @param SubWalkthrough
     */
    public void setPlayerCards(List<PlayerCard> SubWalkthrough) {
        this.playerCards = SubWalkthrough;
    }

    /**
     * Sets winner with defined Winner PlayerCard.
     *
     * @param winner
     */
    public void setWinner(PlayerCard winner) {
        this.winner = winner;
    }
}
