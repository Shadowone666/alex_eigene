/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alex.cardgame.game;

/**
 * represents the board on which is played on
 *
 * @author alexander.lohmann
 */
public class Gamefield {

    /**
     * represents the maximum Round count for this gamefield
     */
    private int maxRound;

    /**
     * represents the trump card on the gamefield
     */
    private Card trump;
    /**
     * represents the stack on the gamefield
     */
    private Cardstack defaultStack;

    /**
     * get the max round count
     *
     * @return max round count
     */
    public int getMaxRound() {
        return maxRound;
    }

    /**
     * get the trump of the gamefield
     *
     * @return trump
     */
    public Card getTrump() {
        return trump;
    }

    /**
     * get the defaultstack of the gamefield
     *
     * @return cardstack
     */
    public Cardstack getDefaultStack() {
        return defaultStack;
    }

    /**
     * Sets the given defaultstack as the defaultstack of this gamefield
     *
     * @param defaultStack
     */
    public void setDefaultStack(Cardstack defaultStack) {
        this.defaultStack = defaultStack;
    }

    /**
     * Checking amount of Points for each Player and consoleoutputs the winner.
     *
     * @param player1Points
     * @param player2Points
     * @return String of the Player who has won
     */
    public String checkWhoWon(int player1Points, int player2Points) {
        if (player1Points > player2Points) {
            return ("Spieler 1 hat gewonnen!");
        } else {
            if (player1Points == player2Points) {
                return ("Unentschieden!");
            } else {
                return ("Spieler 2 hat gewonnen!");
            }
        }
    }

    /**
     * Give out the amount of Cards corresponding with Number of Currentround to
     * all Players.
     *
     * @param currentRound
     * @param player
     */
    public void giveCardsForCurrentRound(int currentRound, Player player) {
        for (int i = 0; i < currentRound; i++) {
            player.addToPlayerHand(defaultStack.takeAndRemoveCard());
        }
    }

    /**
     * Put stack on field. Must be called before start of the round
     */
    public void cardStackToField() {
        defaultStack = Cardstack.buildAndShuffleDefaultStack();
    }

    /**
     * Get Card from stack and add it to trump.
     */
    public void settingTrump() {
        this.trump = defaultStack.takeAndRemoveCard();
    }

    /**
     * Consoleoutput of start of the round.
     */
    public String showRoundStart() {
        return ("######################[Start : Round \n");
    }

    /**
     * Consoleoutput of end of the round.
     *
     * @return
     */
    public String showRoundEnd() {
        return ("Round End ]######################\n\n");
    }
}
