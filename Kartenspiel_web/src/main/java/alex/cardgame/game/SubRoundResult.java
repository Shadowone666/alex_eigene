/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alex.cardgame.game;

import java.util.ArrayList;
import java.util.List;

/**
 * Representing the Results for a single Subround.
 *
 * @author alexander.lohmann
 */
public class SubRoundResult {

    /**
     * Name of the current subround
     */
    private String currentSubRound;
    /**
     * Contains all Fightresults for this Subround
     */
    private List<FightResult> fightresults = new ArrayList<>();

    /**
     * return String containing name of current SubRound.
     *
     * @return String containing name of current SubRound.
     */
    public String getCurrentSubRound() {
        return currentSubRound;
    }

    /**
     * returns the fightresults for current subround.
     *
     * @return the fightresults for current subround.
     */
    public List<FightResult> getFightresults() {
        return fightresults;
    }

    /**
     * Sets the defined Subround name.
     *
     * @param currentSubRound
     */
    public void setCurrentSubRound(String currentSubRound) {
        this.currentSubRound = currentSubRound;
    }

    /**
     * Sets defined fightresults
     *
     * @param fightresults
     */
    public void setFightresults(List<FightResult> fightresults) {
        this.fightresults = fightresults;
    }

}
