/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alex.cardgame.game;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Represents the whole game logic.
 *
 * @author alexander.lohmann
 */
public class Game {

    /**
     * Fightresult for this Game.
     */
    FightResult fightresult;
    /**
     * Current Round for this game.
     */
    private int currentRound;
    /**
     * User Defined amount of Players/Bots.
     */
    private int playerCount;
    private int botNumber;
    /**
     * List of Players.
     */
    private final List<Player> orderedPlayers = new ArrayList<>();
    /**
     * Stringbuilder to that is added every Step of the game as a String for
     * Output.
     */
    private final StringBuilder gameOutput = new StringBuilder();
    /**
     * Max Rounds for this game.
     */
    private int maxRounds;
    /**
     * Information Logger
     */
    private final static Logger L = LoggerFactory.getLogger(Game.class);
    /**
     * Gameresults for this game.
     */
    private GameResult gameresult = new GameResult();

    public int getCurrentRound() {
        return currentRound;
    }

    public int getPlayerCount() {
        return playerCount;
    }

    public int getMaxRounds() {
        return maxRounds;
    }

    /**
     * Getter for OrderedPlayers.
     *
     * @return list of Players
     */
    public List<Player> getOrderedPlayers() {
        return orderedPlayers;
    }

    /**
     * Returns a multiline String representing the result, can only be called
     * after play().
     *
     * @return
     */
    public String getResult() {
        return gameOutput.toString();
    }

    /**
     * Setter for maxRound count.
     *
     * @param maxRounds
     */
    public void setMaxRounds(int maxRounds) {
        this.maxRounds = maxRounds;
    }

    /**
     * Constructor for Game.
     *
     * @param playerCount
     * @param maxRounds
     */
    public Game(int playerCount, int maxRounds) {
        this.playerCount = playerCount;
        this.maxRounds = maxRounds;
        this.botNumber = 1;
    }

    /**
     * Constructor for Game.
     */
    public Game() {
        this.maxRounds = 0;
        this.playerCount = 0;
        this.botNumber = 1;
    }

    /**
     * Resets the Stringbuilder.
     */
    public void resetStringBuilder() {
        gameOutput.delete(0, gameOutput.length() - 1);
    }

    /**
     * Resets the gameresults.
     */
    public void resetGameresult() {
        gameresult = new GameResult();
    }

    /**
     * Creates the amount of Bots that the user defined. Must be called before
     * play().
     */
    public void createPlayers() {
        L.info("Size of the orderedPlayers {}", orderedPlayers.size());
        for (int i = 1; i <= playerCount; i++) {
            createPlayer("Bot" + i);
        }
        L.info("Size after of the orderedPlayers {}", orderedPlayers.size());
    }

    /**
     * Creates a new Player with user defined names.
     *
     * @param name user defined String
     */
    public void createPlayer(String... name) {
        for (String string : name) {
            createPlayer(string);
        }
    }

    /**
     * Creates a new Player and generates an gameoutput for added Player.
     *
     * @param name
     */
    public void createPlayer(String name) {
        Player player = new Player(name);
        orderedPlayers.add(player);
        gameOutput.append("Added player: ").append(player.getName()).append("\n");
    }

    /**
     * Creates a new Human Player with user defined names.
     *
     * @param name that the user defined.
     */
    public void createHumanPlayer(String name) {
        Player player = new Player(name);
        if (!"".equals(name.trim())) {
            player.setBot(false);
            System.out.println(player.getName() + " Hinzugefügt");
        } else {
            player.setName("Bot " + this.botNumber);
            System.out.println(player.getName() + " Hinzugefügt");
            this.botNumber = botNumber + 1;
        }
        orderedPlayers.add(player);
        gameOutput.append("Added player: ").append(player.getName()).append("\n");
        playerCount = playerCount + 1;
    }

    /**
     * Plays the game, must only be called once. Needs defined MaxRoundcount as
     * int and at least 2 Bots as Player.Must be called after createPlayers() or
     * createHumanPlayer() Used for an automatic Playthrough of this Cardgame to
     * test and generate outputs. Sets the Results for a single Playthrough of
     * this Game. Used for generating gameresults containing results of all
     * fights,subRounds,Rounds.
     *
     */
    public void play() {
        //Generating new Gamefield.
        Gamefield game1 = new Gamefield();
        //Putting Cardstack on field
        game1.cardStackToField();
        //Start of round
        for (currentRound = 1; currentRound <= maxRounds; currentRound++) {
            //saves information about this round in roundresults,sets Trump and hands out cards to each player.
            RoundResult roundresult = prepareForRound(game1, currentRound);
            //New PlayerOrder for current Round.
            ArrayList<Player> contestants = setsPlayerListForRound();
            //Start subround
            for (int currentSubRound = 1; currentSubRound <= currentRound; currentSubRound++) {
                //saves some information about this subround in subroundresults.
                SubRoundResult subroundresult = prepareForSubRound(roundresult, currentSubRound);
                //first contestant
                Player contestantOne = setUpAnContestant(contestants);
                //contestant one chooses card. this instance is overwritten by the winner every match.
                PlayerCard contestantOneCard = setUpAnContestantPlayerCard(contestantOne);
                //Start of Fights
                for (int currentFight = 1; currentFight < playerCount; currentFight++) {
                    //next player is choosen and removed from contestants
                    Player contestantTwo = setUpAnContestant(contestants);
                    //next player choose card
                    PlayerCard contestantTwoCard = setUpAnContestantPlayerCard(contestantTwo);
                    //evaluates winner and sets the winner to contestantOne
                    contestantOneCard = runFight(contestantOneCard, contestantTwoCard, roundresult.getCurrentTrump(), subroundresult, currentFight);
                }
                //adds tuck to player and saves information about fight in subroundresult for current subround.
                finishSubRound(contestantOneCard, roundresult, subroundresult);
                //setup Order of player for next subround
                contestants = giveListForNextSubRound(contestantOneCard, contestants);
            }
            //sets Cardstack ,saves results and new order for next round
            setUpForNextRound(game1, roundresult);
        }
    }

    /**
     * return subroundresult for this subround, saves current Round as String
     * and information in gameoutput Must be called after
     * setsPlayerListForRound() and before setUpAnContestant().
     *
     * @param roundresult for current Round
     * @param currentSubRound current Subround count
     * @return subroundresult to save information in for this subround
     */
    public SubRoundResult prepareForSubRound(RoundResult roundresult, int currentSubRound) {
        SubRoundResult subroundresult = new SubRoundResult();
        subroundresult.setCurrentSubRound("SubRound " + currentSubRound);
        return subroundresult;
    }

    /**
     * return ArrayList containing all the Players of the round. Must be called
     * after prepareForRound() and before prepareForSubRound().
     *
     * @return ArrayList containing all the Players of the round.
     */
    public ArrayList<Player> setsPlayerListForRound() {
        //New PlayerOrder for first Round.
        ArrayList<Player> contestants = new ArrayList<>();
        for (Player p : orderedPlayers) {
            contestants.add(p);
        }
        return contestants;
    }

    /**
     * returns the roundresult. Saves Information in RoundResult, deals all the
     * necessary Cards to the players for the Round and sets the Trump. Must be
     * called after Gamefield.cardStacktoField() and before
     * setsPlayerListForRound()
     *
     * @param game1 Gamefield for this game.
     * @param currentRound the currentRoundcount.
     * @return RoundResult for this round.
     */
    public RoundResult prepareForRound(Gamefield game1, int currentRound) {
        RoundResult roundresult = new RoundResult();
        //Consoloutput of roundstart.
        gameOutput.append(game1.showRoundStart());
        roundresult.setCurrentRound("Round " + currentRound);
        //Checking Roundcount and giving same amount of Cards to each player.
        orderedPlayers.forEach((Player player) -> {
            game1.giveCardsForCurrentRound(currentRound, player);
        });
        //set Trump
        game1.settingTrump();
        roundresult.setCurrentTrump(game1.getTrump());
        gameOutput.append("Trump= ").append(roundresult.getCurrentTrump().toColoredString()).append("\n");
        return roundresult;
    }

    /**
     * return Playercard for this contest, containing information of the Player
     * and the played Card. Must be called after an contestant has been
     * initialized.
     *
     * @param player to be one of the Contestants
     * @return Playercard for this contest, containing information of the Player
     * and the played Card.
     */
    public PlayerCard setUpAnContestantPlayerCard(Player player) {
        PlayerCard contestant = new PlayerCard(player, player.playCard(0));
        return contestant;
    }

    /**
     * return Playercard for this contest, containing information of the Player
     * and the played Card. Must be called after an contestant has been
     * initialized.
     *
     * @param player to be one of the Contestants
     * @param card to be setup as Playercard
     * @return Playercard for this contest, containing information of the Player
     * and the played Card.
     */
    public PlayerCard setUpAnContestantPlayerCard(Player player, Card card) {
        PlayerCard contestant = new PlayerCard(player, player.playCard(player.getPlayerHand().indexOf(card)));
        return contestant;
    }

    /**
     * Adds SubRoundResult to current roundResult and adds an tuck to the
     * winner. Must be called after runFight() and before
     * giveListForNextSubRound().
     *
     * @param contestantOneCard winner PlayerCard of SubRound.
     * @param roundresult for current Round
     * @param subroundresult for current SubRound.
     */
    public void finishSubRound(PlayerCard contestantOneCard, RoundResult roundresult, SubRoundResult subroundresult) {
        roundresult.getSubRoundResults().add(subroundresult);
        //add tuck to winner
        contestantOneCard.getPlayer().addPlayerTuck();
    }

    /**
     * Giving an Tuck to the Winner of the SubRound and rearranges the list for
     * the next Subround. Must be called after finishSubRound() and before
     * another SubRound is run.
     *
     * @param contestantOneCard winner PlayerCard
     * @param contestants List of Players for this Round
     * @return ArrayList for next Subround
     */
    public ArrayList<Player> giveListForNextSubRound(PlayerCard contestantOneCard, ArrayList<Player> contestants) {

        //Refills ordered Players with winner on Top.
        int ordinalnumber = orderedPlayers.indexOf(contestantOneCard.getPlayer());
        contestants = new ArrayList<>(orderedPlayers.subList(ordinalnumber, orderedPlayers.size()));
        contestants.addAll(orderedPlayers.subList(0, ordinalnumber));
        return contestants;
    }

    /**
     * Returns a detailed result of the Game.
     *
     * @return a detailed result of the Game.
     */
    public GameResult getDetailResult() {

        return gameresult;
    }

    /**
     * Saves Results and rearranges PlayerLists,sets cardstack for next round.
     * Must be called after giveListForNextSubRound() and before an next Round
     * is run.
     *
     * @param game1 Gamefield of this game
     * @param roundresult for current Round.
     */
    public void setUpForNextRound(Gamefield game1, RoundResult roundresult) {
        //consoloutput of roundend
        gameOutput.append(game1.showRoundEnd());
        //Refilling and shuffeling Carddeck for new Round
        game1.setDefaultStack(Cardstack.buildAndShuffleDefaultStack());
        //new PlayerOrder for next Round
        orderedPlayers.add(orderedPlayers.remove(0));
        gameresult.getResult().add(roundresult);
    }

    /**
     * Plays a Single Fight of the game. And returns the winner PlayerCard of
     * this fight. Must be called after 2 Contestants PlayerCard has been
     * initialized and before finishSubRound() is called.
     *
     * @param contestantOneCard for this fight
     * @param contestantTwoCard for this fight
     * @param trump trump for this round
     * @param subroundresult for current subround
     * @param currentFight count of current Fight
     * @return the winner Playercard of this fight.
     */
    public PlayerCard runFight(PlayerCard contestantOneCard, PlayerCard contestantTwoCard, Card trump, SubRoundResult subroundresult, int currentFight) {
        fightresult = new FightResult();
        fightresult.setCurrentFight("Fight " + currentFight);
        gameOutput.append(contestantOneCard.getPlayer().getName()).append(" played ").append(contestantOneCard.getCard().toColoredString()).append("\n");
        fightresult.getPlayerCards().add(contestantOneCard);
        gameOutput.append(contestantTwoCard.getPlayer().getName()).append(" played ").append(contestantTwoCard.getCard().toColoredString()).append("\n");
        fightresult.getPlayerCards().add(contestantTwoCard);
        //evaluate and save winner
        contestantOneCard = subSubRoundWinner(contestantOneCard, contestantTwoCard, trump);
        //Show WinnerCard
        gameOutput.append(contestantOneCard.getPlayer().getName()).append("  has won with: ").append(contestantOneCard.getCard().toColoredString()).append("\n\n");
        fightresult.setWinner(contestantOneCard);
        subroundresult.getFightresults().add(fightresult);
        return fightresult.getWinner();
    }

    /**
     * Checking played Cards and deciding who has won.
     *
     * @param playercard1
     * @param playercard2
     * @param trump
     * @return the Playercard of who has won for next subSubround
     */
    public PlayerCard subSubRoundWinner(PlayerCard playercard1, PlayerCard playercard2, Card trump) {
        if (playercard1.getCard().getColor() == playercard2.getCard().getColor()) {
            if (playercard1.getCard().getValue() > playercard2.getCard().getValue()) {
                return playercard1;
            } else {
                return playercard2;
            }
        } else {
            if (playercard2.getCard().getColor() == trump.getColor()) {
                return playercard2;
            } else {
                return playercard1;
            }
        }
    }

    /**
     * Gets and removes the Player on Index 0 of that list and returns it. Must
     * be called after prepareForSubRound() and before
     * setUpAnContestantPlayerCard().
     *
     * @param list Any List of Players.
     * @return player with index 0 of that List
     */
    public Player setUpAnContestant(ArrayList<Player> list) {
        Player player = list.get(0);
        list.remove(0);
        return player;
    }
}
