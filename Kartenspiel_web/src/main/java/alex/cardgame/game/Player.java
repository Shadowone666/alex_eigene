/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alex.cardgame.game;

import java.util.ArrayList;

/**
 * Represents the contestants who are playing
 *
 * @author alexander.lohmann
 */
public class Player {

    /**
     * True when Bot , false when Human.
     */
    private boolean bot;

    private String name;

    /**
     * represents the hand of the player
     */
    private ArrayList<Card> playerHand;
    /**
     * represents the points that the player has
     */
    private int points;
    /**
     * represents the tucks that the player has
     */
    private int tucks;

    /**
     * creates new instance of Player
     *
     * @param name
     */
    public Player(String name) {
        this.playerHand = new ArrayList<>();
        this.points = 0;
        this.tucks = 0;
        this.name = name;
        this.bot = true;
    }

    /**
     * retursn true when bot and false when human.
     *
     * @return true when bot and false when human.
     */
    public boolean isBot() {
        return bot;
    }

    /**
     * get the playerhand of the player
     *
     * @return current playerhand
     */
    public ArrayList<Card> getPlayerHand() {
        return playerHand;
    }

    /**
     * get points of the player
     *
     * @return current points of the player
     */
    public int getPoints() {
        return points;
    }

    /**
     * get Tucks of the Player
     *
     * @return current Tucks of the player
     */
    public int getTucks() {
        return tucks;
    }

    public String getName() {
        return name;
    }

    /**
     * Set to false when a player.
     *
     * @param bot
     */
    public void setBot(boolean bot) {
        this.bot = bot;
    }

    /**
     * Sets the Name of the Player/Bot
     *
     * @param name of the Player/Bot
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Adds the Card to the PlayersHand.
     *
     * @param card
     */
    public void addToPlayerHand(Card card) {
        this.playerHand.add(card);
    }

    /**
     * takes the first Card of the Player, removes it from playerhand and
     * returns it.
     *
     * @param index
     * @return PlayerCard
     */
    public Card playCard(int index) {
        if (playerHand.isEmpty()) {
            System.out.println(this.name + " hand EMPTY");
        }
        Card card = this.playerHand.get(index);
        this.playerHand.remove(index);
        return card;
    }

    /**
     * Adds a Point to the Player.
     */
    public void addPlayerPoint() {
        this.points = this.points + 1;
    }

    /**
     * adds a Tuck to the player
     */
    public void addPlayerTuck() {
        this.tucks = this.tucks + 1;
    }

    /**
     * Consoleoutput for each card of the Player.
     */
    public void showHand() {
        System.out.println("PlayerHand :" + hashCode());
        getPlayerHand().forEach((card) -> {
            System.out.print("|" + card.toColoredString() + "| ");
        });
        System.out.println("\n");
    }

}
