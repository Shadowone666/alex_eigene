/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alex.cardgame.game;

import java.util.ArrayList;
import java.util.List;

/**
 * Representing results for all subroundsresults.
 *
 * @author alexander.lohmann
 */
public class RoundResult {

    /**
     * Name of the current round
     */
    private String currentRound;
    /**
     * Variable to safe current Trump in.
     */
    private Card currentTrump;
    /**
     * Contains all subroundresults for current Round.
     */
    private List<SubRoundResult> subRoundResults = new ArrayList<>();

    /**
     * return String with name of currentRound
     *
     * @return String with name of currentRound
     */
    public String getCurrentRound() {
        return currentRound;
    }

    /**
     * return Card representing current Trump
     *
     * @return Card representing current Trump
     */
    public Card getCurrentTrump() {
        return currentTrump;
    }

    /**
     * return List of SubRoundResults for current Round.
     *
     * @return List of SubRoundResults for current Round.
     */
    public List<SubRoundResult> getSubRoundResults() {
        return subRoundResults;
    }

    /**
     * Sets the defined name of current Round.
     *
     * @param currentRounds
     */
    public void setCurrentRound(String currentRounds) {
        this.currentRound = currentRounds;
    }

    /**
     * Sets the defined Trump Card of the current Round.
     *
     * @param currentTrump
     */
    public void setCurrentTrump(Card currentTrump) {
        this.currentTrump = currentTrump;
    }

    /**
     * Sets the defined List of SubRoundResults.
     *
     * @param subRoundResults
     */
    public void setSubRoundResults(List<SubRoundResult> subRoundResults) {
        this.subRoundResults = subRoundResults;
    }
}
