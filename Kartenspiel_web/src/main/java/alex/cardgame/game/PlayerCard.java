/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alex.cardgame.game;

/**
 * Represents the connection between played card and player who played it
 *
 * @author alexander.lohmann
 */
public class PlayerCard {

    /**
     * Represents the player who played the card
     */
    private final Player player;
    /**
     * Represents the card that is played
     */
    private Card card;

    /**
     * Creating new instance of PlayerCard
     *
     * @param player
     * @param card
     */
    public PlayerCard(Player player, Card card) {
        this.player = player;
        this.card = card;
    }

    /**
     * Getting player of the object and returning it
     *
     * @return
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Getting Card of the object and returning it
     *
     * @return
     */
    public Card getCard() {
        return card;
    }
    /**
     * Set Card for this Playercard.
     * @param card 
     */
    public void setCard(Card card) {
        this.card = card;
    }
}
