/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alex.cardgame.game;

import java.util.ArrayList;

/**
 * Represents the Gameresults.
 *
 * @author alexander.lohmann
 */
public class GameResult {

    /**
     * Representing all RoundResult.
     */
    private ArrayList<RoundResult> result = new ArrayList<>();
    /**
     * Representing the game name
     */
    private final String name = "Game";

    /**
     * return String with name of the game.
     *
     * @return String with name of the game.
     */
    public String getName() {
        return name;
    }

    /**
     * return ArrayList containing all roundresult
     *
     * @return ArrayList containing all roundresult.
     */
    public ArrayList<RoundResult> getResult() {
        return result;
    }

    /**
     * Sets result with defined roundresults.
     *
     * @param result
     */
    public void setResult(ArrayList<RoundResult> result) {
        this.result = result;
    }
}
