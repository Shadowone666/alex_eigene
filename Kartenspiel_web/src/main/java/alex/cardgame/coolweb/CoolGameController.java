/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alex.cardgame.coolweb;

import alex.cardgame.game.FightResult;
import alex.cardgame.game.Game;
import alex.cardgame.game.GameResult;
import alex.cardgame.game.PlayerCard;
import alex.cardgame.game.RoundResult;
import alex.cardgame.game.SubRoundResult;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 * Class to be able to run Cardgame on Webserver.
 *
 * @author alexander.lohmann
 */
@Named
@ViewScoped
public class CoolGameController implements Serializable {

    /**
     * Gameresults for this game.
     */
    private GameResult gameresult = new GameResult();
    /**
     * Treenode to output gameresults.
     */
    private TreeNode gameTree;
    /**
     * Errormessage for not enough Bots.
     */
    private String errormessage = "";
    /**
     * Number of user defined Bots.
     */
    private int bots;
    /**
     * Number of user defined maxRounds.
     */
    private int maxRounds;
    /**
     * Botnumber to create game.
     */
    private int bt;
    /**
     * Maxroundsnumber to create game.
     */
    private int mr;
    /**
     * Output of the whole game procedure as a string.
     */
    private String gameoutput = "";
    /**
     * Game and gamelogic.
     */
    private Game game;

    /**
     * Returns Gameresults containing all roundsresults.
     *
     * @return gameresults containing all roundresults.
     */
    public GameResult getGameresult() {
        return gameresult;
    }

    /**
     * Returns current game instance.
     *
     * @return current game.
     */
    public Game getGame() {
        return game;
    }

    /**
     * Returns string for not enough Bots.
     *
     * @return String representing errormessage.
     */
    public String geterrormessage() {
        return errormessage;
    }

    /**
     * User defined amount of Bots.
     *
     * @return number of user defined Bots.
     */
    public int getBots() {
        return bots;
    }

    /**
     * User defined amount of Maxrounds.
     *
     * @return number of user defined maxrounds.
     */
    public int getMaxRounds() {
        return maxRounds;
    }

    /**
     * Returns Game Results as TreeNode with child Treenodes.
     *
     * @return game results as TreeNode with child TreeNodes.
     */
    public TreeNode getGameTree() {
        return gameTree;
    }

    /**
     * Sets the amount of Bots.
     *
     * @param bots
     */
    public void setBots(int bots) {
        this.bots = bots;
    }

    /**
     * public void setGameresult(GameResult gameresult) { this.gameresult =
     * gameresult; } Sets the amount of MaxRounds.
     *
     * @param maxRounds
     */
    public void setMaxRounds(int maxRounds) {
        this.maxRounds = maxRounds;
    }

    /**
     * Info for game config is closed.
     *
     * @param event
     */
    public void onClose(CloseEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Panel Closed", "Closed panel id:'" + event.getComponent().getId() + "'");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Info for game config is opened.
     *
     * @param event
     */
    public void onToggle(ToggleEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, event.getComponent().getId() + " toggled", "Status:" + event.getVisibility().name());
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Setup for the current game with bot amount check.
     */
    public void gameSetup() {
        if (bots < 2) {
            errormessage = "Please Input at least 2 Bots or Player.";
        } else {
            errormessage = "";
            mr = maxRounds;
            bt = bots;

        }
    }

    /**
     * Runs the game and writes it in gameoutput.
     */
    public void mains() {
        game = new Game(bt, mr);
        game.createPlayers();
        if (game == null) {
            errormessage = "Please Setup the Game First <br/>";
        } else {
            game.play();
            gameoutput = game.getResult();
            game.resetStringBuilder();
            System.out.println(gameoutput);
            gameresult = new GameResult();
            gameresult = game.getDetailResult();
            game.resetGameresult();
            buildTreeNode();

        }
    }

    /**
     * Creates new DefaultTreeNode and its childs to save in gameTree
     * Representing the gameresults as TreeNode.
     */
    public void buildTreeNode() {
        gameTree = new DefaultTreeNode(gameresult.getName(), null);
        for (RoundResult roundresult : gameresult.getResult()) {
            DefaultTreeNode roundTreeNode = new DefaultTreeNode(roundresult.getCurrentRound() + ":", gameTree);
            DefaultTreeNode trumpTreeNode = new DefaultTreeNode("Trump = " + roundresult.getCurrentTrump().getColor().name() + " " + roundresult.getCurrentTrump().getValue(), roundTreeNode);
            for (SubRoundResult subroundresult : roundresult.getSubRoundResults()) {
                DefaultTreeNode subRoundTreeNode = new DefaultTreeNode(subroundresult.getCurrentSubRound(), trumpTreeNode);
                for (FightResult fightresult : subroundresult.getFightresults()) {
                    DefaultTreeNode fightsTreeNode = new DefaultTreeNode(fightresult.getCurrentFight(), subRoundTreeNode);
                    DefaultTreeNode winnersTreeNode = new DefaultTreeNode("Winner= " + fightresult.getWinner().getPlayer().getName(), fightsTreeNode);
                    for (PlayerCard playercard : fightresult.getPlayerCards()) {
                        new DefaultTreeNode(playercard.getPlayer().getName() + " played: " + playercard.getCard().getColor().name() + " " + playercard.getCard().getValue(), winnersTreeNode);
                    }
                }
            }
        }
    }
}
