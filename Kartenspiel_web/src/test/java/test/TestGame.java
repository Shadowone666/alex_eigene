/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import alex.cardgame.coolweb.CoolGameController;
import alex.cardgame.game.AllowedColor;
import alex.cardgame.game.Card;
import alex.cardgame.game.Game;
import alex.cardgame.game.Gamefield;
import alex.cardgame.game.Player;
import alex.cardgame.game.PlayerCard;
import alex.cardgame.game.RoundResult;
import alex.cardgame.game.SubRoundResult;
import java.util.ArrayList;
import java.util.List;
import static org.assertj.core.api.Assertions.*;
import org.junit.Before;
import org.junit.Test;

/**
 * Represents the Tests for the Game class.
 *
 * @author alexander.lohmann
 */
public class TestGame {

    /**
     * Represents the current Round.
     */
    int currentRound = 1;
    /**
     * Represents the current sub Round
     */
    int currentSubRound = 1;
    /**
     * Represents the current Fight count.
     */
    int currentFight = 1;
    /**
     * Represents the cool game Controller.
     */
    CoolGameController controller;
    /**
     * Represents the standard game.
     */
    Game game = new Game(2, 2);
    /**
     * Represents the gamefield
     */
    Gamefield gamefield = new Gamefield();
    /**
     * Represents the trump Card.
     */
    Card trump = new Card(7, AllowedColor.RED);
    /**
     * Represents a testGame card to deal to an Player.
     */
    Card testcard = new Card(1, AllowedColor.BLUE);
    /**
     * Represents the standard Player.
     */
    Player player = new Player("Horst");
    /**
     * Represents the first contestantCard.
     */
    PlayerCard contestantOneCard = new PlayerCard(new Player("Herbert"), new Card(10, AllowedColor.GREEN));
    /**
     * Represents the second contestantCard.
     */
    PlayerCard contestantTwoCard = new PlayerCard(new Player("Bärbel"), new Card(5, AllowedColor.YELLOW));
    /**
     * Represents standard List of Players to which players should be added in
     * an Test.
     */
    ArrayList<Player> list = new ArrayList<>();
    /**
     * Represents an roundresult to add information of the round to.
     */
    RoundResult roundresult = new RoundResult();
    /**
     * Represents an subroundresult to add information of the subround to.
     */
    SubRoundResult subroundresult = new SubRoundResult();

    /**
     * Some Test Data.
     */
    @Before
    public void init() {
        int bots = 2;
        int rounds = 1;
        controller = new CoolGameController();
        controller.setBots(bots);
        controller.setMaxRounds(rounds);
        controller.gameSetup();
        game.createPlayers();
        gamefield.cardStackToField();
        list.add(player);
        player.addToPlayerHand(testcard);
    }

    /**
     * Checks if SubRoundResult is added correctly to RoundResult and if an Tuck
     * is added to the Player.
     */
    @Test
    public void isFinishSubRoundValid() {
        game.finishSubRound(contestantOneCard, roundresult, subroundresult);
        assertThat(contestantOneCard.getPlayer().getTucks()).as("should be at least: 1").isGreaterThan(0);
        assertThat(roundresult.getSubRoundResults()).as("Should not be empty").isNotEmpty();
    }

    /**
     * Checks if the FightResult and winner is Valid.
     */
    @Test
    public void isRunFightValid() {
        PlayerCard winner = game.runFight(contestantOneCard, contestantTwoCard, trump, subroundresult, currentFight);
        assertThat(subroundresult.getFightresults().get(0).getCurrentFight()).as("should be: Fight " + currentFight).isEqualTo("Fight " + currentFight);
        assertThat(subroundresult.getFightresults().get(0).getPlayerCards().size()).as("should be: 2").isEqualTo(2);
        assertThat(subroundresult.getFightresults().get(0).getPlayerCards().get(0).getPlayer().getName())
                .as("should be:" + contestantOneCard.getPlayer().getName()).isEqualTo(contestantOneCard.getPlayer().getName());
        assertThat(winner.getPlayer().getName()).as("should be:" + contestantOneCard.getPlayer().getName()).isEqualTo(contestantOneCard.getPlayer().getName());
    }

    /**
     * Checks if the returned PlayerCard is valid and the Card removed from
     * PlayerHand.
     */
    @Test
    public void isSetUpAnContestantPlayerCardValid() {
        PlayerCard playercard = game.setUpAnContestantPlayerCard(player);
        assertThat(player.getPlayerHand().size()).as("should be: 0").isZero();
        assertThat(player.getPlayerHand().size()).as("should be: 0").isNotPositive();
        assertThat(playercard.getPlayer().getName()).as("should be: " + player.getName()).isEqualTo(player.getName());
        assertThat(playercard.getCard().toColoredString()).as("should be:" + testcard.toColoredString()).isEqualTo(testcard.toColoredString());
    }

    /**
     * Checks if the returning Player is Valid and if the Player is removed from
     * the List.
     */
    @Test
    public void isSetUpAnContestantValid() {
        Player listPlayer = game.setUpAnContestant(list);
        assertThat(listPlayer.getName()).as("should be: Horst").isEqualTo("Horst");
        assertThat(list.size()).as("should be: 0").isZero();
    }

    /**
     * Checks the currentSubRound String
     */
    @Test
    public void isprepareForSubRoundValid() {
        roundresult.setCurrentTrump(trump);
        subroundresult = game.prepareForSubRound(roundresult, currentSubRound);
        assertThat(subroundresult.getCurrentSubRound()).as("should be: SubRound " + currentSubRound).isEqualTo("SubRound " + currentSubRound);
    }

    /**
     * Tests if RoundResult,amount of Card for this round on each player and
     * trump for this round is set Valid.
     */
    @Test
    public void isprepareForRoundValid() {
        roundresult = game.prepareForRound(gamefield, currentRound);
        for (Player players : game.getOrderedPlayers()) {
            assertThat(players.getPlayerHand().size()).as("should be:" + currentRound).isEqualTo(currentRound);
        }
        assertThat(roundresult.getCurrentTrump().toColoredString())
                .as("should be:" + gamefield.getTrump().toColoredString()).isEqualTo(gamefield.getTrump().toColoredString());
    }

    /**
     * Checks if the correct amount of Players is in the contestants ArrayList.
     */
    @Test
    public void isPlayerListForRoundValid() {
        List<Player> contestants = game.setsPlayerListForRound();
        assertThat(contestants.size()).as("should be 2").isEqualTo(2);
    }

    /**
     * Tests if the currentSubRound String and Trump set is Valid.
     */
    @Test
    public void isSubRoundResultValid() {
        roundresult.setCurrentTrump(trump);
        subroundresult = game.prepareForSubRound(roundresult, currentSubRound);
        assertThat(subroundresult.getCurrentSubRound()).as("should be:SubRound " + currentSubRound).isEqualTo("SubRound " + currentSubRound);
        assertThat(roundresult.getCurrentTrump().toColoredString()).as("should be:" + trump.toColoredString()).isEqualTo(trump.toColoredString());
    }

    /**
     * Test checks if the Human Player is added and has the correct Name.
     */
    @Test
    public void isHumanPlayerAdded() {
        String name = "Rudolph";
        game.createHumanPlayer(name);
        assertThat(game.getOrderedPlayers().get(game.getOrderedPlayers().size() - 1).getName()).as("should be Rudolph").isEqualTo(name);
    }

    /**
     * Test checks if the correct count of Players is added.
     */
    @Test
    public void isAnPlayerAdded() {
        assertThat(game.getOrderedPlayers().size()).as("should be 2").isEqualTo(2);
    }

    /**
     * Checks if error message is empty String after correct Setup.
     */
    @Test
    public void isGameValid() {
        assertThat(controller.geterrormessage()).as("should be an empty string").isEqualTo("");
    }
}
