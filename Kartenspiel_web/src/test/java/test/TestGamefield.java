/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import static alex.cardgame.game.AllowedColor.RED;
import alex.cardgame.game.Card;
import alex.cardgame.game.Gamefield;
import alex.cardgame.game.Player;
import alex.cardgame.game.PlayerCard;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.assertj.core.api.Assertions.*;
import org.junit.Test;

/**
 *
 * @author alexander.lohmann
 */
public class TestGamefield {

    Gamefield gamefield = new Gamefield();

    @Test
    public void testCheckWhoWon() {
        Player player1 = new Player("Timmy");
        Player player2 = new Player("Rolf");
        player1.addPlayerPoint();
        player1.addPlayerPoint();
        player2.addPlayerPoint();
        String result = gamefield.checkWhoWon(player1.getPoints(), player2.getPoints());
        assertThat(result).as("Should be: Spieler 1 hat gewonnen!").isEqualTo("Spieler 1 hat gewonnen!");
    }

    @Test
    public void testGiveCardsForCurrentRound() {
        Player player1 = new Player("Timmy");
        gamefield.cardStackToField();
        int currentRound = (int) (Math.random() * 10);
        gamefield.giveCardsForCurrentRound(currentRound, player1);
        assertThat(player1.getPlayerHand().size()).as("Should be an Size of " + currentRound).isEqualTo(currentRound);
    }

}
